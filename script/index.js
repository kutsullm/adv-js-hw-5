import API from "./API/API.js"
import Card from "./Card/Card.js"
const div = document.getElementById("root")


async function init (){
const data = await API.getData("/users")
const posts = await API.getData("/posts")
const cards = posts.map((item)=>{
    let obj = {}
    data.forEach(element => {
        if(item.userId === element.id){
         obj = {...item,author:element}
        }
    });
  return obj
})
 Card.render(cards,div)
}
init()

