class API{
    constructor(){
        this.baseUrl = "https://ajax.test-danit.com/api/json"
    }
    async getData(url){
     const res = await fetch(`${this.baseUrl}${url}`)
     const data = await res.json()
     return data
    }
    async deleteData(url,id){
     const res = await fetch(`${this.baseUrl}${url}/${id}`,{
        method:"DELETE"
  
     })
     if(res.status === 404){
        return res
     }

    }
    async createData(url,data){
        
    }
    async updateData(url,data,id){
        
    }
}

export default new API