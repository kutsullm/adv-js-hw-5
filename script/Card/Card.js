import button from "../button/button.js"
import API from "../API/API.js"
class Card {
  constructor() {

  }
  render(data, parent) {
    parent.insertAdjacentHTML("beforeend", `<ul>${data.map((item) => {
      return this.renderCard(item)

    }).join("")}</ul>`)
    data.forEach(element => {
      const li = document.getElementById(element.id)
      const btn = button.renderButton("delete", this.handleDelete)
      li.append(btn)
    });
  }
  renderCard({ title, body, author, id }) {
    return `<li id = ${id}><h2>${title}</h2><p>${body}</p><p>${author.name}</p><p>${author.email}</p></li>`
  }

  async handleDelete(event) {
    event.preventDefault()
    const li = event.target.closest("li")
    const id = li.id
    const res = await API.deleteData("/posts", id)
    li.remove()
  }
  
}
export default new Card