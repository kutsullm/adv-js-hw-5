import { buttonConfig } from "./config.js";

class Button{
    constructor(){

    }
    renderButton(key,onClick){
        const  {type,text,className} = buttonConfig[key]
        const btn = document.createElement('button')
        btn.addEventListener('click', onClick)
        btn.type = type
        btn.className =className
        btn.textContent=text
        this.btn=btn
        return this.btn
        // return `<button onClick = ${onClick} class =${className} type = ${type}>${text}</button>`
    }
}

const button = new Button()
export default button